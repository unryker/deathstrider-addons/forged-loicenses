### Notes
---
- An addon for [Deathstrider.](https://gitlab.com/accensi/deathstrider)
- A quick workaround that gives you all licenses for free at the start of each new game by default.
- Recommended for players that don't want to remember to pick up licenses from the flagship every time.
- Configurable! Check out the options in `Deathstrider Settings > Addon Menus > Forged Loicenses`.